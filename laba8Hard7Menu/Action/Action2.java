package laba8Hard7Menu.Action;

import java.util.Scanner;

public class Action2 extends Action{
  String message = "Если вам нужно сложить два числа, то выбирайте этот пункт";

  public Action2(String message){
    super(message);
  }

  public void act() throws Exception{
    int first = checkingInt("Введите первое слагаемое");
    int second = checkingInt("Введите второе слагаемое");
    int result = first + second;
    System.out.printf("Наслаждайтесь результатом: %d",result);
    System.out.println();
  } 

  public Integer checkingInt(String str) throws Exception{
  Scanner in = new Scanner(System.in);
  boolean flag = true;
  while (flag){
    System.out.println(str);
    String s = in.nextLine();
    int a;
    try{
      a = Integer.parseInt(s);
      System.out.println();
      return a;
    }catch(Exception e){}
  }
  return null;
  }

}

package laba8Hard7Menu.Action;

import java.util.Scanner;
import java.util.Random;

public class Action5 extends Action{
  String message = "Предстоит сложное решение? Подбрось монетку!";  

  public Action5(String message){
    super(message);
  }

  public void act() throws Exception{
    int coin = checkingInt("Орёл или решка? Орёл - введите 0, решка - введите - 1:", 0, 1); //константы для метода проверки ввода и цикла ввода
    Random rnd = new Random();
    int res = rnd.nextInt(2);
    if (coin == res){
      System.out.print("Вы выиграли!   ");
    }
    else{
      System.out.print("Увы, не сегодня :(   ");
    }
  }

  public Integer checkingInt(String str, int min, int max) throws Exception{
  Scanner in = new Scanner(System.in);
  boolean flag = true; // для вайла
  while (flag){
    System.out.println(str);
    String s = in.nextLine();
    int a;
    try{
      a = Integer.parseInt(s);
      if (a >= min && a <= max){
        System.out.println();
        return a;
      }
    }catch(Exception e){}
  }
  return null;
  }

}

package laba8Hard7Menu.Action;

import java.util.InputMismatchException;

import java.util.Scanner;

public class Action4 extends Action{
  String message = "Если вам нравятся квадраты, может, вычислим квадрат вашего любимого числа?";

  public Action4(String message){
    super(message);
  }

  public void act() throws InputMismatchException{
    int number = checkingInt("Введите число для возведения в квадрат:");
    int result = number * number;
    System.out.printf("Вот и квадрат: %d",result);
    System.out.println();  
  }

  public Integer checkingInt(String str) throws InputMismatchException{
  Scanner in = new Scanner(System.in);
  boolean flag = true;
  while (flag){
    System.out.println(str);
    String s = in.nextLine();
    int a;
    try{
      a = Integer.parseInt(s);
      System.out.println();
      return a;
    }catch(Exception e){}
  }
  return null;
  }
}

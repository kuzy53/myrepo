package laba8Hard7Menu;
import java.util.Scanner;
import laba8Hard7Menu.Action.*;

public class Main{
  public static void main(String[] args)  throws Exception{
    Scanner in = new Scanner(System.in);
    int userInput = 0; //по дефолту на старте
    Question start = new Question();    //инициализируем узлы дерева  
    Question level1 = new Question();
    Question level1_2 = new Question();
    Question level2 = new Question();
    Question level2_2 = new Question();
    Action one = new Action1("Проверить, счастливый ли я");
    start.goNext(level1);
    start.goNext(level1_2);
    start.doTask(one);
    level1.doTask(new Action2("Поиграть в сложение"));
    level1.goNext(level2);
    level1.goNext(level2_2);
    level1_2.doTask(new Action3("Поиграть в умножение"));
    level2.doTask(new Action4("Поиграть в квадрат"));
    level2_2.doTask(new Action5("Подбросить монетку"));

    while(true){
      start.consolOutput();
      userInput = in.nextInt();
      System.out.println();
      if (userInput == 0){
        if (start.parent == null){
          break;
        }
        else start = start.parent;
      }
      if (userInput == 3){
        start.actions.get(userInput - 3).act();
      }
      if(userInput == 1 && start.children.size() == 0){
        start.actions.get(userInput - 1).act();
      }
      if(userInput > 0 && userInput < start.actions.size() + start.children.size()){
        start = start.children.get(userInput - start.children.size() + 1);
      }
    }
  }
}

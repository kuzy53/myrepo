package l8t3;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class SortArrayTest {
    @Test void checkCreatedSortedArr(){
      double[] target = new double[]{0.0,3.0,6.0,8.0,9.0};
      double[] arrtotest = new double[]{3.0,6.0,9.0,8.0,0.0};
      assertArrayEquals(target, SortArray.createSortedArray(arrtotest), "created sorted array not ok");
      assertFalse(target == SortArray.createSortedArray(arrtotest));
      }


    @Test void checkCreatedSortedEmpArr(){
      double[] target = new double[]{};
      double[] arrtotest = new double[]{};
      assertArrayEquals(target, SortArray.createSortedArray(arrtotest), "created sorted empty array not ok");
    }

    @Test void checkSort(){
      double[] arrtotest = new double[]{3.0,6.0,9.0,8.0,0.0};
      SortArray.sort(arrtotest);
      double[] target = new double[]{0.0,3.0,6.0,8.0,9.0};
      assertArrayEquals(target, arrtotest, "sort method not ok");
      }

      @Test void checkEmptySort(){
        double[] arrtotest = new double[]{};
        SortArray.sort(arrtotest);
        double[] target = new double[]{};
        assertArrayEquals(target, arrtotest, "empty sort method array not ok");
      }

}

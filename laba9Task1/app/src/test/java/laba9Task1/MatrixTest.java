package laba9Task1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class MatrixTest {

    @Test void trySetGetMatrix() throws NotNsizeMatrixException {
        Matrix matrix = new Matrix(2, 2);
        int[][] m1 = new int[][]{{8, 6}, {1, 5}};
        matrix.setMatrix(m1);
        assertArrayEquals(m1, matrix.getMatrix());
    }

    @Test void tryNotRightSizeMatrix() throws NotNsizeMatrixException {
        Matrix matrix = new Matrix(5,5);
        int[][] m1 = new int[][]{{8},{9}};
        NotNsizeMatrixException e = assertThrows(NotNsizeMatrixException.class, () -> matrix.setMatrix(m1));
        assertTrue(e.getMessage().contains("Матрица задана неверно"));
    }

    @Test void trySetNullMatrix() throws NotNsizeMatrixException {
        Matrix matrix = new Matrix(2,2);
        int[][] m1 = new int[][]{{}, {}};
        NotNsizeMatrixException e = assertThrows(NotNsizeMatrixException.class, () -> matrix.setMatrix(m1));
        assertTrue(e.getMessage().contains("Матрица задана неверно"));
    }

    // делала тест на исключение по аналогии с этим

    // @Test void exceptionTesting() {
    //   MyException thrown = assertThrows(
    //      MyException.class,
    //      () -> myObject.doThing(),                       // лямбда-функция
    //      "Expected doThing() to throw, but it didn't"
    //   );
    //   assertTrue(thrown.getMessage().contains("Stuff"));
    // }
}

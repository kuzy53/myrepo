package structurs;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;

public class MyStackTest {

    MyStack<Integer> stack = new MyStack<>(10);
    Integer i = 3;


    @Test void push() {
        stack.push(i);
        assertEquals(i, stack.peek());
    }

    @Test void pop() {
        stack.push(i);
        stack.push(2);
        stack.push(4);
        stack.pop();
        assertEquals(2, stack.peek());
    }

    @Test void full() {
        assertFalse(stack.full());
    }

    @Test void empty() {
        assertTrue(stack.empty());
    }

    @Test void size() {
        stack.push(2);
        assertEquals(1, stack.size());
    }

    @Test void isEmpty() {
        assertTrue(stack.isEmpty());
    }

    @Test
    void contains() {
        stack.push(2);
        assertTrue(stack.contains(2));
        assertFalse(stack.contains(4));
    }
    

    @Test
    void checkEmptyIterator(){
        MyStackTest[] arr1 = new MyStackTest[]{};
        MyStackTest[] arr = new MyStackTest[]{};
        Iterator iterator = stack.iterator();
        int i = 0;
        while (iterator.hasNext()) {
            MyStackTest retValue = (MyStackTest)iterator.next();
            arr[i] = retValue;
            i++;
        }
        assertArrayEquals(arr, arr1, "work correct");
    }


    @Test
    void remove() {
        stack.push(2);
        stack.push(4);
        assertEquals(4, stack.remove());
    }

    @Test
    void add() {
        stack.add(3);
        assertEquals(3, stack.peek());
    }

    @Test
    void removeObject() {
        stack.add(1);
        stack.add(2);
        assertEquals(2, stack.remove());
    }

    @Test
    void clear() {
        stack.add(1);
        stack.add(2);
        stack.clear();
        stack.add(1);
        assertEquals(1, stack.peek());
    }
}

package laba9Task2;

class Vector{

  private double x;
  private double y;
  private double x1;
  private double y1;

  Vector(double x, double y, double x1, double y1) {
    this.x = x;
    this.y = y;
    this.x1 = x1;
    this.y1 = y1;
  }

  Vector(Vector vctr) {
    this.x = vctr.getX();
    this.y = vctr.getY();
    this.x1 = vctr.getX1();
    this.y1 = vctr.getY1();
  }

  public void setX(double x) {
    this.x = x;
  }

  public double getX() {
    return this.x;
  }

  public void setY(double y) {
    this.y = y;
  }

  public double getY() {
    return this.y;
  }

  public void setX1(double x1) {
    this.x1 = x1;
  }

  public double getX1() {
    return this.x1;
  }

  public void setY1(double y1) {
    this.y1 = y1;
  }

  public double getY1() {
    return this.y1;
  }

  public String print(){
    return "Начало вектора (" + this.x + ";" + this.y + ")" + "\n" +
           "Конец вектора (" + this.x1 + ";" + this.y1 + ")";
  }

  public void add(Vector vector2){
    this.x1 += this.getXX1();
    this.y1 += this.getYY1();
  }

  public void substraction(Vector vector2){
    this.x1 -= this.getXX1();
    this.y1 -= this.getYY1();
  }

  public void multiply(int a){
    x *= a;
    y *= a;
    y1 *= a;
    x1 *= a;
  }

  public void divide(int a){
    this.y /= a;
    this.x /= a;
    this.y1 /= a;
    this.x1 /= a;
  }

  public double getXX1() {
    double xx1 = this.x1 - this.x;
    return xx1;
  }

  public double getYY1() {
    double yy1 = this.y1 - this.y;
    return yy1;
  }


  static Vector sum(Vector vector1, Vector vector2){
    Vector result = new Vector(vector1);
    result.add(vector2);
    return result;
  }

  public static Vector sub(Vector vector1, Vector vector2){
    Vector result = new Vector(vector1);
    result.substraction(vector2);
    return result;
  }

  public static Vector multi(Vector vector1, int a){
    Vector result = new Vector(vector1);
    result.multiply(a);
    return result;
  }

  public static Vector div(Vector vector1, int a){
    Vector result = new Vector(vector1);
    result.divide(a);
    return result;
  }

}

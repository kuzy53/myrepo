package stredit;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class MyStackTest2 {
      MyStack<Integer> stack = new MyStack<>(10);
      Integer i = 3;

  @Test
  void containsAll() {
      List<Integer> list = new ArrayList<>();
      int i = 2;
      int x = 5;
      list.add(i);
      list.add(x);
      stack.add(i);
      stack.add(x);
      assertTrue(stack.containsAll(list));
  }


  @Test
  void addAll() {
      List<Integer> list = new ArrayList<>();
      int i = 2;
      int x = 5;
      list.add(i);
      list.add(x);
      stack.addAll(list);
      assertEquals(5, stack.peek());
  }

  @Test
  void removeAll() {
      List<Integer> list = new ArrayList<>();
      int i = 2;
      int x = 5;
      list.add(i);
      list.add(x);
      stack.push(2);
      stack.push(3);
      stack.push(5);
      stack.removeAll(list);
      assertEquals(3, stack.peek());
  }

  @Test
  void retainAll() {
      List<Integer> list = new ArrayList<>();
      int i = 2;
      int x = 5;
      list.add(i);
      list.add(x);
      stack.push(3);
      stack.push(5);
      stack.push(2);
      stack.retainAll(list);
      assertEquals(2, stack.peek());
  }
}

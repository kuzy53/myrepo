package laba8Easy3;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class NumbersTest {
    @Test void trySumOfNumbers(){
      assertEquals(1770, Numbers.sumOfNumbers(59), "trySumOfNumbers works incorrect");
    }

    @Test void trySumOfNumbers0(){
      assertEquals(0, Numbers.sumOfNumbers(0), "trySumOfNumbers0 works incorrect");
    }
}

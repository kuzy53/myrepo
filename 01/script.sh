#!/usr/bin/bash
#создаем файл, чтобы записывать в него данные
> data
exec 6>&1
exec 1>README.md
#делаем ввод данных
read INPUT
k=0
#записываем время начала
VARStart="$(date)"
#цикл while
while [[ $k -lt "2**$INPUT" ]]
do 
echo $(( 2**$k ))
let $(( k=$k+1 ))
echo $k
done
#записываем время конца скрипта
VAR="$(date)"
echo "$VARStart"
echo "$VAR"
#выходим из файла
exec 1>&-
exec 1>&6
exec 6>&-
#выводим файл в строку
cat README.md



package four;
import java.util.Scanner;
import java.time.*;

public class RandomProcess {

  public static double generateNumber(double min, double max){
    double generatedNumber = (Math.random()*((max-min)+1))+min;
    // System.out.println(generatedNumber);
    return generatedNumber;
  }

  public static long generateTime(long maxTime) {
    long leftLimit = 1000;
    long rightLimit = maxTime;
    long generatedLong = leftLimit + (long) (Math.random() * (rightLimit - leftLimit));
    return generatedLong;
  }

  public static void main(String[] args) throws InterruptedException{
    Scanner sc = new Scanner(System.in);
    double range = sc.nextDouble();
    double coefficient = sc.nextDouble();
    long maxTime = sc.nextLong();

    long start = System.currentTimeMillis();
    do{
      System.out.println(generateNumber(-(Math.abs(range)), Math.abs(range)));
      Thread.sleep(generateTime(maxTime));
    }
    while(generateNumber((-(Math.abs(range))), Math.abs(range)) > range);

    long finish = System.currentTimeMillis();
    long total = finish - start;
    System.out.println("Время от начала эксперимента: " + total);
  }


}

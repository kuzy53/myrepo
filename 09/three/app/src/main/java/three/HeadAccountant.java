package three;

public class HeadAccountant extends Worker{

  HeadAccountant(String name, String position, int salary){
    super(name, position, salary);
  }

  public void countAccSalary(){
      System.out.println("I gave our accountant salary!");
    }
}

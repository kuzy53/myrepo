package laba9Task4;

public class Act {
  public static void main(String[] args) throws FigureNotExistException, NotTriangleException, WrongEdgesException {
    int[] arr = new int[] {3, 3, 3};
    Triangle triangle = new Triangle(arr);
    System.out.println(triangle.get2Side());

    int[] arr1 = new int[] {2, 2, 3};
    Triangle triangle1 = new Triangle(arr1);
    System.out.println(triangle1.get2Side());

    Text text0 = new Text("hahahaha!!!");
    int[] arrForFigure0 = new int[]{3, 5, 3, 5};
    Figure figure0 = new Figure(arrForFigure0);
    int[] arrForTriangle0 = new int[]{7, 8, 9};
    Triangle triangle0 = new Triangle (arrForTriangle0);
    Mutual arr4[] = new Mutual[]{text0, figure0, triangle0};
    //вывожу массив объектов через print
    for (int i = 0; i < arr4.length; i++){
      System.out.println(arr4[i].print());
    }
    System.out.println();
    //вывожу массив объектов через printAll
    System.out.println(Mutual.printAll(arr4));
  }
}

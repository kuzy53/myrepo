package laba9Task4;

public class FigureNotExistException extends Exception{
  FigureNotExistException(){
    super("Фигура с заданными параметрами не существует");
  }
}
